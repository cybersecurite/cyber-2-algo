```mermaid
flowchart LR
    id1[[Contribuer au site cybersécurité]]
```

Tout d'abord, merci de l'intéret que vous portez à ce projet et de prendre du temps pour contribuer.

Toutes les formes de contributions sont encouragées et valorisées. N'hésitez pas à lire ci-dessous pour découvrir les différentes façons de nous aider et comment nous allons les accueillir. N'hésitez pas à lire la partie concernant la nature de votre contribution, cela nous facilitera la vie et nous permettra une meilleure réponse. Nous attendons vos contributions.

> Si vous aimez le projet mais n'avez pas le temps de contribuez, ce n'est pas grave. Vous pouvez toutefois nous aider :
> - Mettre une étoile au projet
> - En parler sur les réseaux sociaux
> - Faire un lien dans vos projets vers celui-ci
> - Parler de ce projet à vos collègues, amis, ...



## Table des matières

- [Code de Conduite](#code-de-conduite)
- [J'ai une question](#j-ai-une-question)
- [Je veux contribuer](#je-veux-contribuer)
- [Nous-rejoindre](#nous-rejoindre)


## Code de Conduite

Ce projet et ses contributions sont guidés par les principes de contribution applicables à tous. En particulier, nous attendons un comportement et une communication non aggressives. Nous ne tolérerons pas de marque de discrimination fondées sur la valeur individuelle des contributeurs.


## J'ai une question

> Avant de poser une question, nous considérons que vous avez lu la [documentation](https://forge.apps.education.fr/cybersecurite/cyber/-/wikis/home).

Avant de poser une question, assurez-vous d'avoir relu les [tickets](https://forge.apps.education.fr/cybersecurite/cyber/-/issues) qui pourraient vous aider. Si malgré tout vous n'avez pas trouvé de réponse à votre question, vous pouvez l'écrire sous forme d'un ticket.


## Je veux contribuer

> ### Legal Notice 
> Lorsque vous contribuez à ce projet, vous devez vous assurer que vous disposez des droits à le faire. En particulier, vous devez être l'auteur à 100% de votre contribution.

### Suggérer ou signaler des améliorations

Vous devrez généralement ouvrir un ticket dans les situations suivantes:
- créer des challenges
- discuter d’un sujet ou d’une idée
- restructurer les mises en page pour améliorer la convivialité du projet
- écrire et améliorer la documentation du projet
- demander si vous pouvez aider à écrire une nouvelle fonctionnalité
- signaler une erreur

Un ticket = votre message  + un responsable + un statut

il peut s’enrichir : un ticket = votre message + un responsable + un statut + une priorité + un délai + des questions/réponses + des pièces jointes.

Après avoir choisi votre projet et cliqué sur [Tickets](https://forge.apps.education.fr/cybersecurite/cyber/-/issues) > Liste > [Nouveau ticket](https://forge.apps.education.fr/cybersecurite/cyber/-/issues/new), vous arrivez sur l’interface de composition.

### Comment bien rédiger un ticket ?

Bien qualifier votre demande permet d’obtenir un traitement plus rapide et efficace. C’est simple !

- Pour que le système soit efficace, rédigez une seule demande par ticket.
Cela permet une assignation et une résolution plus rapide, un suivi plus clair, un historique propre.

- Décrivez simplement votre demande, par ex : "quand je clique sur le menu, les entrées ne sont pas triées par ordre alphabétique" 

- Copiez/collez-nous l’adresse (URL) où le problème se trouve, plutôt que "Sur la page contact…"

Dans ce cas,nous vous recommandons au choix plusieurs possibilités.

```mermaid
flowchart TD
    B[" Tickets "]
    B-->C(Tickets personnalisés)
    B-->D(Tickets à remplir)
    B-->E(Mail)
```

1. tickets personnalisés

La première étape consiste à écrire un titre explicite et à choisir un modèle de ticket.

![ticketA](docs/images/ticketsA.png)

Ensuite compléter le ticket. 

![ticketB](docs/images/ticketsB.png)

Vous pouvez avoir un visuel de ce ticket en cliquant sur aperçu.

![ticketC](docs/images/ticketsC.png)

Il est possible de mettre en forme de façon plus précise le ticket 

![ticketD](docs/images/ticketsD.png)

Et ne pas oublier de valider le ticket en cliquant sur créer ticket

![ticketE](docs/images/ticketsE.png)



2. tickets vierges

- créer un [ticket](/-/issues/new).
- Rédigez votre message comme un e-mail
- Utilisez la barre de mise en forme. Vous pouvez utiliser la syntaxe [Markdown](https://docs.gitlab.com/ee/user/markdown.html) pour gagner du temps
- Fournir le plus d'informations possibles
- Attachez des fichiers et images en les faisant glisser dans la fenêtre de saisie
- N’oubliez pas de cliquer sur Créer ticket pour enregistrer !


3. Créer des tickets par e-mail

Nous écrire à l'adresse suivante et le message qui deviendra un ticket
forge-apps+guichet+cybersecurite-cyber-910-issue-@phm.education.gouv.fr


Nous reviendrons vers vous le plus rapidement.


## Nous rejoindre

N'hésitez pas à nous rejoindre en adhérant à l'[AEIF](https://aeif.fr) en précisant que vous souhaitez contribuer à ce projet de façon régulière.

## Attribution

Ce guide est basé sur **contributing.md**. [Créez le vôtre](https://contributing.md/) !

