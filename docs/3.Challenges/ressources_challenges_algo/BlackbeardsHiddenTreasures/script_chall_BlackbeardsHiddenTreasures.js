const flags = {'BlackbeardsHiddenTreasures1':['A3',1,1], // [flag, numéro de la partie dans le challenge, partie suivante ou pas]
                'BlackbeardsHiddenTreasures2':['E6',2,1],
                'BlackbeardsHiddenTreasures3':['K6',3,1],
                'BlackbeardsHiddenTreasures4':['F9',4,1],
                'BlackbeardsHiddenTreasures5':['A6',5,1],
                'BlackbeardsHiddenTreasures6':['B9',6,1],
                'BlackbeardsHiddenTreasures7':['J4',7,1],
                'BlackbeardsHiddenTreasures8':['N4',8,1],
                'BlackbeardsHiddenTreasures9':['K5',9,1],
                'BlackbeardsHiddenTreasures10':['C2',10,1],
                'BlackbeardsHiddenTreasures11':['F7',11,1],
                'BlackbeardsHiddenTreasures12':['A1',12,0]};

function check_and_update(event)
{
    event.preventDefault();

    const f = event.target;
    const f_name = f.id.substring(5); // Extraction de la clé du dictionnaire dans le nom du formulaire
    const texte = document.getElementById('Texte'+flags[f_name][1].toString());
    if (f.flag.value===flags[f_name][0]) // Réussite à un challenge
	{
		texte.innerHTML = 'Bravo ! Flag validé !';
        texte.style.color = 'black';
        if(flags[f_name][2]===0) // Réussite du dernier challenge
		{
			document.getElementById('Final').hidden = false;
		}
		else
		{
			const val_part = flags[f_name][1]+flags[f_name][2];
			document.getElementById('Partie'+val_part.toString()).hidden = false;
		}
	}
	else 
	{
		texte.innerHTML = 'Flag incorrect ! Réessayez !';
        texte.style.color = 'red';
	}
}

// formulaires commencent par form_. formulaire de la forme : form_ + clé du dictionnaire flags
form_BlackbeardsHiddenTreasures1.addEventListener( "submit", check_and_update);
form_BlackbeardsHiddenTreasures2.addEventListener( "submit", check_and_update);
form_BlackbeardsHiddenTreasures3.addEventListener( "submit", check_and_update);
form_BlackbeardsHiddenTreasures4.addEventListener( "submit", check_and_update);
form_BlackbeardsHiddenTreasures5.addEventListener( "submit", check_and_update);
form_BlackbeardsHiddenTreasures6.addEventListener( "submit", check_and_update);
form_BlackbeardsHiddenTreasures7.addEventListener( "submit", check_and_update);
form_BlackbeardsHiddenTreasures8.addEventListener( "submit", check_and_update);
form_BlackbeardsHiddenTreasures9.addEventListener( "submit", check_and_update);
form_BlackbeardsHiddenTreasures10.addEventListener( "submit", check_and_update);
form_BlackbeardsHiddenTreasures11.addEventListener( "submit", check_and_update);
form_BlackbeardsHiddenTreasures12.addEventListener( "submit", check_and_update);