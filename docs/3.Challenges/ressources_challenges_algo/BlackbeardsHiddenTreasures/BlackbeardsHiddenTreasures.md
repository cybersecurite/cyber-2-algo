---
hide:
  - toc
author: à compléter
title: Blackbeard’s Hidden Treasures
---

# Challenge : Blackbeard’s Hidden Treasures


![](../pirate-flag.png){: .center }

The period of the late 17th and early 18th centuries is known as the Golden Age of Piracy. During this period the most notorious and the most feared of all pirates was Blackbeard. There are many legends based around Blackbeard’s piracy acts in the Caribbean sea and in the Pacific Ocean but the most captivating legend is based around Blackbeard’s great buried treasure, which has yet to be found.

A small treasure chest, full of parchments has recently been found by a team of historians who were researching the Golden Age of piracy at The Codrington Library, Oxford UK. The parchments were hidden within the inside cover of an ancient book about Myths and Legends of the Golden Age of Piracy.

It would seem that Blackbeard may not have buried his treasure in one single location but may instead **have split his treasure and buried it using twelve different locations**.

!!! note "Your task"
    Use the twelve parchments, as well as the treasure map provided below to locate all the content of Blackbeard’s treasure.

    <center><img src = '../map.png' style='width:40%;height:auto;'></center>

    Each parchment provides a string of characters of the form _letter+number_, such as `B6``. The flag is this string.

??? abstract "Help for string manipulations"

    `string = "abcdef"`

    `string[0]` would return `"a"`, the character in the string at position `0`

    `string[1]` would return `"b"`, the character in the string at position `1`

    `string[2]` would return `"c"`, the character in the string at position `2`

    etc.

    `LENGTH(string)` would return the number of characters in the string (e.g. `6`)

    `LEFT(string,3)` would return the first three characters of the string (e.g. `"abc"`)

    `RIGHT(string,3)` would return the last three characters of the string (e.g. `"def"`)

??? abstract "Help for `MOD` & `DIV` operators"

    `DIV` is the quotient of a division, whereas `MOD` is the remainder.

    For instance: `20 / 3 = 6` remainder `2`

    So:

    `20 DIV 3` would return `6`

    `20 MOD 3` would return `2`

??? abstract "Help for ASCII Codes"

    Check the <a href='https://www.101computing.net/wp/wp-content/uploads/ASCII-Table.pdf'>following ASCII table</a> to lookup the right codes.

<hr style="height:5px;color:red;background-color:red;">
<center>It's your turn!</center>
<hr style="height:5px;color:red;background-color:red;">

<div id='Partie1'>
<h2>First parchment</h2>
    <pre>
    x = "A"
    y = "B"
    z = x
    i = 15
    j = 2 * i /10
    column = z
    row = j
    OUTPUT column + row
    </pre>

<p>
    <form id="form_BlackbeardsHiddenTreasures1">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte1'></p>
</div>


<div id='Partie2' hidden>
<br>
<h2>Second parchment</h2>
    <pre>
    code = "treasure"
    column = code[2]
    row = LENGTH(code)
    row = row - 2
    OUTPUT column + row
    </pre>

<p>
    <form id="form_BlackbeardsHiddenTreasures2">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte2'></p>
</div>



<div id='Partie3' hidden>
<br>
<h2>Third parchment</h2>
    <pre>
    longitude = 39
    lattitude = 45
    IF lattitude < longitude THEN
        row = longitude - lattitude
        column = "C"
    ELSE
        row = lattitude - longitude
        column = "K"
    END IF
    OUTPUT column + row
    </pre>

<p>
    <form id="form_BlackbeardsHiddenTreasures3">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
<p>
    <p id='Texte3'></p>
</div>

<div id='Partie4' hidden>
<br>
<h2>Fourth parchment</h2>
    <pre>
    row = 1
    FOR i FROM 1 TO 4
        row = row + 2
    END FOR
    message = "Pirates of the Carribean"
    column = message[row]
    OUTPUT column + row
    </pre>

<p>
    <form id="form_BlackbeardsHiddenTreasures4">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte4'></p>
</div>

<div id='Partie5' hidden>
<br>
<h2>Fifth parchment</h2>
    <pre>
    row = 3
    message = "Atlantic Ocean"
    FOR i FROM 1 TO 4
        row = row + i
    END FOR
    row = row DIV 2
    column = message[3]
    OUTPUT column + row
    </pre>

<p>
    <form id="form_BlackbeardsHiddenTreasures5">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte5'></p>
</div>

<div id='Partie6' hidden>
<br>
<h2>Sixth parchment</h2>
    <pre>
    FUNCTION square(number)
        RETURN number * number
    END FUNCTION

    row = square(3)
    column = LEFT("Blackbeard", 1)
    OUTPUT column + row
    </pre>

<p>
    <form id="form_BlackbeardsHiddenTreasures6">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte6'></p>
</div>

<div id='Partie7' hidden>
<br>
<h2>Seventh parchment</h2>
    <pre>
    position = "Long. 37.4, lat. 112.9"
    longitude = LEFT(position,10)
    lattitude = RIGHT(position,10)
    row = RIGHT(longitude,1)
    IF LENGTH(position) > 10 THEN
        column = "J"
    ELSE
        column = "G"
    END IF
    OUTPUT column + row
    </pre>

<p>
    <form id="form_BlackbeardsHiddenTreasures7">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
<p>
    <p id='Texte7'></p>
</div>

<div id='Partie8' hidden>
<br>
<h2>Eighth parchment</h2>
    <pre>
    code = "Pacific Ocean"
    row = 0
    WHILE code[row] != "f"
        row = row + 1
    END WHILE
    column = RIGHT(code,1)
    OUTPUT column + row
    </pre>

<p>
    <form id="form_BlackbeardsHiddenTreasures8">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte8'></p>
</div>

<div id='Partie9' hidden>
<br>
<h2>Ninth parchment</h2>
    <pre>
    column = CHR("75")
    row = ASCII("A")
    IF column == "K" THEN
        row = row - 60
    ELSE
        row = row + 60
    END IF
    OUTPUT column + row
    </pre>

<p>
    <form id="form_BlackbeardsHiddenTreasures9">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte9'></p>
</div>

<div id='Partie10' hidden>
<br>
<h2>Tenth parchment</h2>
    <pre>
    letters = ["G", "K", "C", "E", "J", "D"]
    column = letters[0]
    FOR i FROM 0 TO 5
        IF letters[i] < column THEN
            column = letters[i]
            row = i
        END IF
    END FOR
    OUTPUT column + row
    </pre>

<p>
    <form id="form_BlackbeardsHiddenTreasures10">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte10'></p>
</div>

<div id='Partie11' hidden>
<br>
<h2>Eleventh parchment</h2>
    <pre>
    row = 10
    FOR i FROM 0 TO 10 STEP 2
        row = row + 10
    END FOR
    column = ASCII(row)
    row = row / 10
    OUTPUT column + row
    </pre>

<p>
    <form id="form_BlackbeardsHiddenTreasures11">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte11'></p>
</div>

<div id='Partie12' hidden>
<br>
<h2>Twelfth parchment</h2>
    <pre>
    high = 14
    low = 8
    WHILE high > low
        low = low + 1
        high = high - 1
    END WHILE
    row = high MOD 2
    column = RIGHT(LEFT("pirate",4),1)
    OUTPUT column + row
    </pre>

<p>
    <form id="form_BlackbeardsHiddenTreasures12">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
<p>
    <p id='Texte12'></p>
</div>

<div id='Final' markdown="1" hidden>
<br>
<h2>Congratulations!</h2>
<p>Blackbeard's treasure is ours!</p>
<center><img src='../tresor.png' style='width:40%;height:auto;'></center>
<br>

!!! danger "A retenir"

    De nombreux codes informatiques implémentés dans diverses applications plus ou moins sensibles admettent à l'heure actuelle des failles de sécurité représentant de ce fait un vecteur d'attaque pour les pirates. Une bonne maîtrise et une bonne compréhension de l'algorithmique peut permettre d'éviter ces problèmes en produisant des codes informatiques sûrs.

</div>

<script src='../script_chall_BlackbeardsHiddenTreasures.js'></script>