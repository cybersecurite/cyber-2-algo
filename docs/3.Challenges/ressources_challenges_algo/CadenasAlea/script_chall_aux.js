/* Récupération du code généré et création du digicode */
import {CODE,create_digicode} from "./digicode.js"

/* Fonction pour choisir un élément aléatoire dans un tableau */
function choice(array){ return array[Math.floor(Math.random() * array.length)]};

/* crée un code aléatoire en choisissant length caractères de la liste alphabet */
function randomString(length, alphabet) {
    let result = '';
    while (result.length < length) {
      result += alphabet.charAt(Math.floor(Math.random() * alphabet.length));
    }
    return result;
}

/* endors le programme pendant ms millisecondes.
   Doit être appelé avec: await sleep(...).
   Voir: https://www.sitepoint.com/delay-sleep-pause-wait/#howtowritebettersleepfunctioninjavascript
*/
function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}


/* Les flags du challenge sont générés aléatoirement à chaque exécution de la page. */
const flags = { // clé form_clé [flag, numéro de la partie dans le challenge, partie suivante ou pas]
    'c1':[ choice(["poire","banane","abricot","chocolat","crunchies","raisintine", "profiterole"]),1,2], //Ici on choisit un flag aléatoire parmi les trois possibles.
    'c2':[ CODE, 2, 3 ],

    'c3':[ randomString(4, '0123456789'), 3, 0 ] //On crée un code aléatoire à chaque exécution de la page de longueur 5
}

//console.log(flags)

/* On crée le "mot caché" pour le premier challenge */
document.getElementById('dessert').textContent = '*'.repeat(flags['c1'][0].length) //La chaîne écrite est de la même longueur que le code, mais elle ne contient que des *

/* On crée le "cadenas" du challenge 3 comme une suite de boutons n'acceptant que des chiffres */

document.getElementById('cadenas-hint').innerHTML = '<input type="text" maxlength="1" pattern="[0-9]" class="lock-letter" placeholder="?">'.repeat(flags['c3'][0].length)



/* On attendra un délai fixé "facilement" reconnaissable */
const SLEEP_TIME = choice([300,500,800,1000]);

/* On ajoute un écouteur sur l'input ou l'utilisateur tape son code */
document.getElementById('cadenas-submit').addEventListener( "click", async (event) => {
    event.preventDefault();

    //On passe le résultat à calcul en cours, avec le css
    document.getElementById('cadenas-result').textContent='Calcul en cours';
    document.getElementById('cadenas-result').className="loading";

    //On attend un petit peu pour que l'utilisateur remarque
    await sleep(1000);

    //On récupère la liste des boutons où l'utilisateur a tapé son code
    const combiUser = document.getElementById('cadenas-hint').children;

    //Et le code du cadenas
    const combiCadenas = flags['c3'][0];

    let counter = 0;

    //Pour chaque chiffre de la combinaison
    for (let i = 0; i < combiCadenas.length; i++){

	//Si la valeur dans le champ i est différente du chiffre i de la combinaison
	if (combiUser[i].value != combiCadenas[i]) {
	    
	    //On attend un certain temps.
	    const time = i*SLEEP_TIME/1000
        await sleep(SLEEP_TIME*i)

        //On affiche une erreur
	    document.getElementById('cadenas-result').className="error"
	    document.getElementById('cadenas-result').textContent = "Code Faux, testé en " + time + " s.";

	    //On a pas besoin de comparer plus loin
	    return false;
	}
    }
    
    //Si on est arrivé jusque ici, alors la combinaison est la bonne.
    document.getElementById('cadenas-result').className="success"
    document.getElementById('cadenas-result').textContent = "Bravo vous avez trouvé la bonne combinaison !";
    return true;
}
							  )

/* On crée le digicode pour le deuxième challenge */
create_digicode()

function check_and_update(event)
{
    event.preventDefault();

    const f = event.target;
    const f_name = f.id.substring(5); // Extraction de la clé du dictionnaire dans le nom du formulaire
    const texte = document.getElementById('Texte'+flags[f_name][1].toString());
    if (f.flag.value===flags[f_name][0]) // Réussite à un challenge
    {
	texte.innerHTML = 'Bravo ! Flag validé !';
        texte.style.color = 'black';
        if(flags[f_name][2]===0) // Réussite du dernier challenge
	{
	    document.getElementById('Final').hidden = false;
	}
	else
	{
	    const val_part = flags[f_name][2];
	    document.getElementById('Partie'+val_part.toString()).hidden = false;
	}
    }
    else 
    {
	texte.innerHTML = 'Flag incorrect ! Réessayez !';
        texte.style.color = 'red';
    }
}

// formulaires commencent par form_. formulaire de la forme : form_ + clé du dictionnaire flags
form_c1.addEventListener( "submit", check_and_update);
form_c2.addEventListener( "submit", check_and_update);
form_c3.addEventListener( "submit", check_and_update);
