const IS_SUCCESS = 'is-success';
const IS_ERROR = 'is-error';
const DELAY = 1000;
export const CODE = generateCode(4);
let builder = '';

const display = /** @type {HTMLDivElement} */ (document.getElementById('display'));
const light = /** @type {HTMLDivElement} */ (document.getElementById('light'));
const keypad = /** @type {HTMLDivElement} */ (document.getElementById('keypad'));

/** @type {number|null} */
let taskId = null;

/**
 * @param {number} len
 * @returns {string}
 * On crée un code len nombres compris entre 0 et 9.
 */
function generateCode(len) {
    let code = '';
    for (let i = 0; i < len; i++) {
	code += Math.floor(Math.random() * 10).toString();
    }
    return code;
}

/**
 * Supprime le code affiché et enlève les classes pour que la lampe soit éteinte.
 */

function resetCode() {
    builder = '';
    display.innerText = '_'.repeat(CODE.length);
    light.classList.remove(IS_SUCCESS, IS_ERROR);
}

/**
 * @param {number} digit
 * Pour chaque bouton du digicode, il ajoutera un chiffre au code et le bon nombre de _ aux emplacements suivants.
 * On met à jour l'état de la lampe selon si le code est un préfixe du bon code.
 */
function composeDigit(digit) {
    if (taskId !== null) {
	clearTimeout(taskId);
	taskId = null;
    }
    if (builder.length < CODE.length) {
	builder += digit.toString();
    }
    display.innerText = builder + '_'.repeat(CODE.length - builder.length);
    light.classList.remove(IS_SUCCESS, IS_ERROR);
    if (CODE.startsWith(builder)) {
	light.classList.add(IS_SUCCESS);
    }
    else {
	light.classList.add(IS_ERROR);
	taskId = setTimeout(resetCode, DELAY);
    }
    
}

/**
 * On crée les 10 boutons (0 à 9) et chacun est associé à une instance de la fonction composeDigit avec le chiffre associé.
 */
export function create_digicode(){
    for (let i = 1; i < 11; i++) {
	const digit = i % 10;
	const button = document.createElement('button');
	button.innerText = digit.toString();
	button.onclick = () => composeDigit(digit); // Fermeture qui appelle composeDigit avec le chiffre associé au bouton.
	keypad.appendChild(button);
    }
    
    resetCode(); // On affiche rien dans l'interface du code par défaut.
}
