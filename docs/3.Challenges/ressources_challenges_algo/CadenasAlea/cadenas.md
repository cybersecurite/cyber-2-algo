---
hide:
  - toc
author: à compléter
title: cadenas dynamiques
---

# Challenge : cadenas dynamiques

<link rel="stylesheet" href="../cadenas.css">


![](../cadenas.png){: .center }



!!! note "Votre objectif"
    Trouver les différents codes cachés, attention ils sont tous aléatoires.

<hr style="height:5px;color:red;background-color:red;">
<center>A vous de jouer !</center>
<hr style="height:5px;color:red;background-color:red;">

<section id="Partie1">
<h2> Une mise en jambe </h2>

<p>
    Le groupe arrive devant la serre des forêts tropicales humides de ce lieu magnifique.
	A l'entrée il y a un écran sur lequel est inscrit ce message énigmatique.
<br><br>
	Tout les matins je choisis un dessert aléatoirement parmi une <em>poire</em>, une <em>banane</em>, un <em>abricot</em>,  du <em>chocolat</em>, des <em>crunchies</em>, une <em>raisintine</em> ou bien une <em>profiterole</em>.
Pour cela je tire dans un choixpeau un papier où est écrit le dessert.
</p>
<br>

<p>Ce matin je vais manger <span id="dessert"></span> !</p>
<br>
<p>
Après quelques minutes de réflexion, le petit Marius se dirige vers l'écran pour entrer le nom du dessert et la porte s'ouvre. Le Dr Kalifir et le Dr Kalifounet se regardent perplexes. 
</p>

<p>
<form id="form_c1">
	<label for="flag">Code trouvé : </label>
	<input type="text" name="flag" style='background-color:black;color:white;' size=50>
	<input type="submit" value="Vérifier">
</form></p>

<p id="Texte1"></p>

</section>

<section id="Partie2" hidden>
<br>
<h2>Un digicode très bien fait</h2>
 
<p>Ils continuèrent leur chemin pour aboutir devant une autre porte avec un digicode sur la gauche. Une fois que vous aurez trouvé le bon code, entrez le dans le formulaire en dessous du digicode.</p>
<p>
Après quelques essais, le petit Marius trouve le code. Le Dr Kalifir et le Dr Kalifounet se regardent dubitatifs. 
</p>

<div id="digicode">
<div id="output">
<div id="display"></div>
<div id="light"></div>
</div>
<div id="keypad"></div>
</div>


<p>
<form id="form_c2">
	<label for="flag">Code trouvé : </label>
	<input type="text" name="flag" style='background-color:black;color:white;' size=50>
	<input type="submit" value="Vérifier">
</form></p>

<p id='Texte2'></p>

</section>

<section id="Partie3" hidden>
<br>
<h2>Un code un peu plus complexe</h2>

Après avoir ouvert la porte à l'aide du digicode, les trois compères se retrouvent face à un nouveau cadenas numérique, plutôt récalcitrant.
<br>
Le Dr Kalifir et le Dr Kalifounet laissent tout de suite le petit Marius devant ce nouveau défi.
<br>
Après quelques essais, le petit Marius trouve une nouvelle fois le code. Le Dr Kalifir et le Dr Kalifounet regardent celui-ci avec admiration. 


<div id="cadenas">
<div id="cadenas-hint"></div>
<input type="button" id="cadenas-submit" value="Déverrouiller"/>
<div id="cadenas-result"></div>
</div>

<p>
<form id="form_c3">
	<label for="flag">Code trouvé : </label>
	<input type="text" name="flag" style='background-color:black;color:white;' size=50>
	<input type="submit" value="Vérifier">
</form></p>
<p id='Texte3'></p>
</section>

<section id='Final' markdown="1" hidden>
<br>
<h2> Félicitations !</h2>

<p>

Vous avez réussi à résoudre les trois défis.
<br>

!!! danger "A retenir"
	Ces trois défis correspondent au <strong><a href="https://fr.wikipedia.org/wiki/Principe_de_Kerckhoffs#:~:text=161%E2%80%93191%2C%20F%C3%A9vrier%201883%20),'adversaire%20conna%C3%AEt%20le%20syst%C3%A8me%20%C2%BB." target="_blank">deuxième principe de Kerchoffs</a></strong>, bien connu en cybersécurité. Il s'oppose à la <strong><a href="https://fr.wikipedia.org/wiki/S%C3%A9curit%C3%A9_par_l%27obscurit%C3%A9" target="_blank">sécurité par l'obscurité</a></strong>.

	Ce principe énonce (globalement) que: <q>La sécurité d'un algorithme ne doit reposer <strong>que sur le secret de la clef</strong></q>.

	Encore faut-il pour cela que l'algorithme ne fasse rien fuiter sur cette clé...

</p>
</section>


<script type="module" src='../script_chall_aux.js'></script>