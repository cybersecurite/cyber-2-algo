---
hide:
  - toc
author: à compléter
title: Cadenas
---

# Challenge : cadenas


![](../coffre_cadenas.png){: .center }

Après des années de recherche, vous venez enfin de localiser un coffre contenant un incroyable trésor. Arrivé sur 
place, vous le découvrez dans une pièce murée située dans les souterrains d'un antique château en ruine.

En l'observant de plus près, vous vous apercevez que son système d'ouverture est incroyablement complexe. Heureusement, 
vous découvrez également à côté du coffre un parchemin couvert de mystérieux symboles et figures.

Après avoir ramené le tout dans votre Institut d'Archéologie à laquelle vous êtes affilié, vous vous enfermez dans votre 
bureau afin d'étudier le parchemin. Après plusieurs heures, vous avez enfin terminé sa traduction : il s'agit de la 
description de cinq puzzles de codage qui, une fois résolus, vous permettront d'ouvrir enfin le coffre...

!!! note "Votre objectif"
    Résoudre les cinq puzzles de codage


<hr style="height:5px;color:red;background-color:red;">
<center>A vous de jouer !</center>
<hr style="height:5px;color:red;background-color:red;">

<div id='Partie1'>
<h2>Puzzle 1</h2>
<p>Code à trois chiffres :
<ul>
    <li><code>913</code> : aucun chiffre n’est bon</li>
    <li><code>280</code> : deux chiffres sont bons mais un seul est bien placé</li>
    <li><code>047</code> : deux chiffres sont bons mais mal placés</li>
    <li><code>426</code> : aucun chiffre n’est bon</li>
    <li><code>587</code> : deux chiffres sont bons mais mal placés</li>
</ul></p>

<p>
    <form id="form_coffre_cadenas1">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte1'></p>
</div>


<div id='Partie2' hidden>
<br>
<h2>Puzzle 2</h2>
<p>Code à trois chiffres :
<ul>
    <li><code>216</code> : aucun chiffre n’est bon</li>
    <li><code>385</code> : un chiffre est bon mais mal placé</li>
    <li><code>367</code> : un chiffre est bon et bien placé</li>
    <li><code>743</code> : deux chiffres sont bons mais mal placés</li>
    <li><code>264</code> : un chiffre est bon mais mal placé</li>
</ul></p>

<p>
    <form id="form_coffre_cadenas2">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte2'></p>
</div>


<div id='Partie3' hidden>
<br>
<h2>Puzzle 3</h2>
<p>Code à trois chiffres :
<ul>
    <li><code>021</code> : deux chiffres sont bons mais mal placés</li>
    <li><code>794</code> : un chiffre est bon mais mal placé</li>
    <li><code>094</code> : aucun chiffre n'est bon</li>
    <li><code>168</code> : un chiffre est bon et bien placé</li>
    <li><code>920</code> : un chiffre est bon mais mal placé</li>
</ul></p>

<p>
    <form id="form_coffre_cadenas3">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte3'></p>
</div>


<div id='Partie4' hidden>
<br>
<h2>Puzzle 4</h2>
<p>Code à quatre chiffres :
<ul>
    <li><code>3781</code> : deux chiffres sont bons mais mal placés</li>
    <li><code>0273</code> : trois chiffres sont bons et un seul est mal placé</li>
    <li><code>1245</code> : aucun chiffre n'est bon</li>
    <li><code>6807</code> : trois chiffres sont bons mais mal placés</li>
</ul></p>

<p>
    <form id="form_coffre_cadenas4">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte4'></p>
</div>


<div id='Partie5' hidden>
<br>
<h2>Puzzle 5</h2>
<p>Code à quatre chiffres :
<ul>
    <li><code>9862</code> : deux chiffres sont bons mais un seul est bien placé</li>
    <li><code>5261</code> : trois chiffres sont bons mais mal placés</li>
    <li><code>6785</code> : trois chiffres sont bons mais deux sont mal placés</li>
</ul></p>

<p>
    <form id="form_coffre_cadenas5">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte5'></p>
</div>





<div id='Final' markdown="1" hidden>
<br>
<h2>Félicitations !</h2>
<p>Vous avez résolu les cinq puzzles de codage !<br><br>Le trésor est à vous !</p>
<center><img src='../coffre_cadenas_tresor.png' style='width:40%;height:auto;'></center>
<br>

!!! danger "A retenir"

    La mise en place des concepts abstraits de l'algorithmique passe par une phase de compréhension et de réflexion de ces concepts. Sans cette phase importante, ces concepts sont souvent mal compris ce qui conduit le plus souvent, lors de l'implémentation machine, à écrire des programmes informatiques comportant des failles de sécurité qui sont ensuite exploitées par les pirates.

</div>

<script src='../script_chall_coffre_cadenas.js'></script>